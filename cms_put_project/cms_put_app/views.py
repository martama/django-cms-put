from django.shortcuts import render
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt

formulario = """
<p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""


# Create your views here.
@csrf_exempt
def seacrh(request, llave):
    if request.method == "PUT":
        cuerpo = request.body.decode('utf-8')
        try:
            """Si el valor ya lo teníamos en nuestra base de datos,
            actualizarlo al nuevo valor que venga
            en el cuerpo de la petición."""
            content = Contenido.objects.get(clave=llave)
            content.valor = cuerpo
            content.save()
            respuesta = "Has actualizado el valor del contenido '" \
                        + llave + "' que ya existía a '" \
                        + cuerpo + "'."
            return HttpResponse(respuesta)
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=cuerpo)
            c.save()
            """si la llave está en contenido, que nos devuelva su valor"""
            content = Contenido.objects.get(clave=llave)
            respuesta = "El valor, para el contenido '" + llave \
                        + ", se ha registrado como '" \
                        + content.valor + "'."
            return HttpResponse(respuesta)
    if request.method == "POST":
        """cuerpo = request.body.decode('utf-8')"""
        cuerpo = request.POST['valor']
        c = Contenido(clave=llave, valor=cuerpo)
        c.save()

        try:
            """si llave está en Contenido, que nos devuelva su valor"""
            conten = Contenido.objects.get(clave=llave)
            respuesta = "El valor, para su búsqueda: " + llave \
                        + ", es: " + conten.valor
        except Contenido.DoesNotExist:
            respuesta = "No hay contenido para su búsqueda " \
                        + llave + formulario
        return HttpResponse(respuesta)
